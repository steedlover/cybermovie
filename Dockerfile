#Download base image ubuntu 16.04
FROM ubuntu:18.04

RUN apt-get -y update && apt-get -y install curl
RUN curl -sL https://deb.nodesource.com/setup_10.x  | bash -
RUN apt-get -y install nodejs

VOLUME "./:/var/www/"

WORKDIR /var/www/

COPY . .

RUN chmod +x ./docker/startup.sh

CMD "./docker/startup.sh"

EXPOSE 8080
