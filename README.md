# The Cybermovie application

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.29.

## Starting the server

#### In development mode

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.
The app will automatically reload if you change any of the source files.

#### From the docker container

Run `docker pull dimoz/cybermovie:latest` for downloading the image

To start the container of the image, run `docker run -p 8080:8080 --detach --name cybermovie dimoz/cybermovie`

The app will be accessible on `http://localhost:8080`

If the container starts first time it could take about 5 minutes to download all the dependencies and make the production version of the app

`docker stop cybermovie` to stop the container

I selected the `ubuntu` image as base cause it turned out the `node` image size is almost a gigabyte!
I had tried both of them before made this desicion, besides it's easier to configure and install necessary dpendencies.

## A couple of thoughts regarding the application

- I have two variables as a state of the app. The `loading` (hide/show the spinner) and `active genre`.
  The `loading` I put in the redux storage but with `active genre` I work through the genre service.
  In terms of immutability of data both of these ways are good enough so which one is better, depends on task
  but here I used both of them for learning purposes.  
  I used to use the redux storage for dynamic structures. For instance [here](https://bitbucket.org/steedlover/esp-server/src/7b2bdc34cf1602bbbafff66f7eb3e29fc39c3353/service/state.js?at=master)
  I have dynamic list of some entities (`machines`- ESP8622 controllers) and I can add and remove elements from the array.
  In this case it totally makes sense to use redux, but in case of the static movies list it doesn't. That's why
  I don't use it to store the movies itself. But for learning purpose I use redux to hide/show the spinner.

- I tried to store the `current genre` variable to the local storage. For this I installed the `ngx-localstorage`
  It would allow me to show selected genre on returning from the detailed page. But it was not easy within my approach
  of the floating data in the `movie-list` component. It would not allow me to display the `home` page with the top
  rated movies properly.  
  Basically, I should have been made a separate component for the `home` page. But I realized that in the very end
  and had decided not to waste time.

- I left some comment through the code. You can easily find them searching by the `COMMENT` keyphrase.
