#!/bin/sh

if [ ! -d "/var/www/node_modules" ] || [ ! "$(ls -a /var/www/node_modules)" ]; then
  rm -rf /var/www/node_modules
  cd /var/www
  echo y | npm install
  # install web server
  echo y | npm install -g angular-http-server
  # Install CLI
  echo y | npm install -g @angular/cli
fi

# Build the project
ng build --prod

# go to dist directory
cd dist/cybermovie

# Start web server
angular-http-server -p 8080
