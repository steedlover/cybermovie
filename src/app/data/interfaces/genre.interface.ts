import { TGenre } from "../types/genre.type";

export interface IGenre {
  name: TGenre;
  icon: string;
}
