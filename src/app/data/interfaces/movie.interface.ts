import { TGenre } from "../types/genre.type";

export interface IMovie {
  id: number;
  key: string;
  name: string;
  description: string;
  genres: TGenre[];
  rate: string;
  length: string;
  img: string;
}
