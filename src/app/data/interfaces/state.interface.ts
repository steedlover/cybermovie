export interface IStateResponse {
  state: IState;
}

export interface IState {
  activeGenre: string;
  loading: boolean;
}
