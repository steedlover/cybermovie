import { Observable } from "rxjs";
import { Injectable } from "@angular/core";
import { IMovie } from "../interfaces/movie.interface";
import { movies } from "../../static/movie.list";
import { Store } from "@ngrx/store";
import { MovieListConfig } from "../../static/config";

@Injectable()
export class MovieService {
  constructor(private store: Store) {}

  getAllMovies() {
    return new Observable<IMovie[]>((observer) => {
      this.store.dispatch({ type: "LOADING_ON" });
      setTimeout(() => {
        this.store.dispatch({ type: "LOADING_OFF" });
        observer.next(movies);
      }, 500);
    });
  }

  getMovieImageCSSUrlAttr(obj: IMovie): string {
    let url: string = "";
    if (obj.img && typeof obj.img === "string") {
      url += MovieListConfig.imagesPath + obj.img;
    }
    return url ? 'url("' + url + '")' : "";
  }

  getMovieByAlias(alias: string) {
    return new Observable<IMovie>((observer) => {
      this.store.dispatch({ type: "LOADING_ON" });
      setTimeout(() => {
        this.store.dispatch({ type: "LOADING_OFF" });
        let result;
        const resultArr = movies.filter((el) => el.key === alias);
        if (resultArr.length > 0) {
          observer.next(resultArr[0]);
        } else {
          observer.error("Not found");
        }
      }, 500);
    });
  }
}
