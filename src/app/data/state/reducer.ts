import { Action } from "@ngrx/store";
import { IState } from "../interfaces/state.interface";

let initialState = {
  activeGenre: "",
  loading: false,
};

const newStateHelper = (state: IState, newData: any) => {
  return Object.assign({}, state, newData);
};

export function rootReducer(state: IState = initialState, action: Action) {
  console.log(action.type, state, "reducer call");
  switch (action.type) {
    case "LOADING_ON":
      return newStateHelper(state, { loading: true });

    case "LOADING_OFF":
      return newStateHelper(state, { loading: false });

    default:
      return state;
  }
}
