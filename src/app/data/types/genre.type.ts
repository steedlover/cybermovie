export type TGenre =
  | "action"
  | "adventure"
  | "biography"
  | "comedy"
  | "crime"
  | "drama"
  | "history"
  | "mystery"
  | "scifi"
  | "sport"
  | "thriller";

export const genreTypes = {
  action: "action" as TGenre,
  adventure: "adventure" as TGenre,
  biography: "biography" as TGenre,
  comedy: "comedy" as TGenre,
  crime: "crime" as TGenre,
  drama: "drama" as TGenre,
  history: "history" as TGenre,
  mystery: "mystery" as TGenre,
  scifi: "scifi" as TGenre,
  sport: "sport" as TGenre,
  thriller: "thriller" as TGenre,
};
