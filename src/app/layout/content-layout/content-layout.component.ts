import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { map } from "rxjs/operators";
import { IGenre } from "src/app/data/interfaces/genre.interface";
import { SlideAnimationBouncing } from "../slide.animation";

@Component({
  selector: "app-content-layout",
  animations: [SlideAnimationBouncing],
  templateUrl: "./content-layout.component.html",
  styleUrls: ["./content-layout.component.scss"],
})
export class ContentLayoutComponent implements OnInit {
  private _genres: IGenre[];
  public sidePanelState: string = "hidden";

  constructor(private route: ActivatedRoute) {}

  set genres(g: IGenre[]) {
    if (!this._genres && g) {
      setTimeout(() => (this.sidePanelState = "visible"), 0);
    }
    this._genres = g;
  }

  get genres(): IGenre[] {
    return this._genres;
  }

  ngOnInit() {
    this.route.data
      .pipe(map((data) => data.genres))
      .subscribe((data: IGenre[]) => (this.genres = data));
  }
}
