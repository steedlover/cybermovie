import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ContentLayoutComponent } from "./content-layout/content-layout.component";
import { GenreResolver } from "./resolvers/genre.resolver";

const routes: Routes = [
  // COMMENT
  // We could have different layouts here such as Auth, Error etc.
  // Every layout could have had different set of components such as footer, header, sidebar etc.
  {
    path: "",
    component: ContentLayoutComponent,
    children: [
      {
        path: "",
        loadChildren: () =>
          import("../modules/movie/movie.module").then((m) => m.MovieModule),
      },
    ],
    resolve: { genres: GenreResolver },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [GenreResolver],
})
export class LayoutRoutingModule {}
