import { NgModule } from "@angular/core";

import { SharedModule } from "../shared/shared.module";

import { NavbarComponent } from "../layout/navbar/navbar.component";
import { BurgerButtonComponent } from "../layout/navbar/burger-button/burger-button.component";
import { ContentLayoutComponent } from "./content-layout/content-layout.component";
import { SideBarComponent } from "./side-bar/side-bar.component";
import { ActiveGenreService } from "../shared/services/active-genre.service";
import { LayoutRoutingModule } from "./layout-routing.module";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

@NgModule({
  imports: [
    LayoutRoutingModule,
    SharedModule,
    BrowserModule,
    BrowserAnimationsModule,
  ],
  exports: [],
  providers: [ActiveGenreService],
  declarations: [
    ContentLayoutComponent,
    SideBarComponent,
    NavbarComponent,
    BurgerButtonComponent,
  ],
})
export class LayoutModule {}
