import {
  Component,
  Input,
  Inject,
  Renderer2,
  RendererFactory2,
  OnInit,
  OnDestroy,
} from "@angular/core";
import { DOCUMENT } from "@angular/common";
import { Router } from "@angular/router";
import { IGenre } from "../../data/interfaces/genre.interface";
import { ActiveGenreService } from "../../shared/services/active-genre.service";
import {
  SlideAnimationSmooth,
  SlideAnimationBouncing,
} from "../slide.animation";
import { NavBarConfig } from "../../static/config";
import { Subscription } from "rxjs";
import { TGenre } from "../../data/types/genre.type";

@Component({
  selector: "app-navbar",
  animations: [SlideAnimationSmooth, SlideAnimationBouncing],
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.scss"],
})
export class NavbarComponent implements OnInit, OnDestroy {
  public mobileMenuClosed: boolean = true;
  public mobileMenuActive: boolean = false;
  public activeGenre: TGenre;
  public navBarState: string = "hidden";
  public navMenuState: string = "hidden";
  public navBarHeight: number;
  private renderer: Renderer2;
  private subscriptions: Subscription[] = [];

  @Input() genres: IGenre[] = [];

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private rendererFactory: RendererFactory2,
    private genre: ActiveGenreService
  ) {
    this.navBarHeight = NavBarConfig.navBarHeight;
  }

  animComplete(e) {
    const newState: string = e.toState;
    this.mobileMenuClosed = newState === "hidden" ? true : false;
  }

  burgerClicked(s: boolean) {
    if (s !== this.mobileMenuActive) {
      this.navMenuState = s === false ? "hidden" : "visible";
      this.mobileMenuActive = s;
      if (this.mobileMenuActive === true) {
        this.renderer.addClass(this.document.body, "modal-open");
      } else {
        this.renderer.removeClass(this.document.body, "modal-open");
      }
    }
  }

  setActive(name: TGenre) {
    if (this.activeGenre !== name) {
      this.genre.active = name;
    }
    this.burgerClicked(false);
  }

  ngOnInit() {
    setTimeout(() => (this.navBarState = "visible"), 0);
    this.renderer = this.rendererFactory.createRenderer(null, null);
    this.subscriptions.push(
      this.genre.activeGenreObserver.subscribe((n: TGenre) => {
        if (this.activeGenre !== n) {
          this.activeGenre = n;
        }
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }
}
