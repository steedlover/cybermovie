import { TestBed } from "@angular/core/testing";

import { GenreResolver } from "./genre.resolver";

describe("GenreResolver", () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it("should be created", () => {
    const service: GenreResolver = TestBed.get(GenreResolver);
    expect(service).toBeTruthy();
  });
});
