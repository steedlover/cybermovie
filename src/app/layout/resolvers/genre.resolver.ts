import { Injectable } from "@angular/core";
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from "@angular/router";
import { Observable } from "rxjs";
import { IGenre } from "../../data/interfaces/genre.interface";
import { genreList } from "../../static/genre.list";
import { Store } from "@ngrx/store";

@Injectable({
  providedIn: "root",
})
export class GenreResolver implements Resolve<IGenre[]> {
  constructor(private store: Store) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<IGenre[]> | Promise<IGenre[]> | IGenre[] {
    return new Observable((o) => {
      this.store.dispatch({ type: "LOADING_ON" });
      setTimeout(() => {
        this.store.dispatch({ type: "LOADING_OFF" });
        o.next(genreList);
        o.complete();
      }, 1000);
    });
  }
}
