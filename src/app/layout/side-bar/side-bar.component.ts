import { Component, Input, OnInit, OnDestroy } from "@angular/core";
import { Router } from "@angular/router";
import { IGenre } from "../../data/interfaces/genre.interface";
import { ActiveGenreService } from "../../shared/services/active-genre.service";
import { Subscription } from "rxjs";
import { TGenre } from "../../data/types/genre.type";

@Component({
  selector: "app-side-bar",
  templateUrl: "./side-bar.component.html",
  styleUrls: ["./side-bar.component.scss"],
})
export class SideBarComponent implements OnInit, OnDestroy {
  public activeGenre: TGenre;
  private subscriptions: Subscription[] = [];

  @Input() genres: IGenre[] = [];

  constructor(private genre: ActiveGenreService, private router: Router) {}

  setActive(name: TGenre) {
    if (this.activeGenre !== name) {
      this.genre.active = name;
    }
  }

  ngOnInit() {
    this.subscriptions.push(
      this.genre.activeGenreObserver.subscribe((n: TGenre) => {
        if (this.activeGenre !== n) {
          this.activeGenre = n;
        }
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }
}
