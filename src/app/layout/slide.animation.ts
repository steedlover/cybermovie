import {
  trigger,
  state,
  style,
  transition,
  animate,
} from "@angular/animations";

export const SlideAnimationBouncing = [
  trigger("bounceSlideAnim", [
    state(
      "hidden",
      style({
        transform: "{{translateHidden}}",
      }),
      { params: { translateHidden: "translateX(-100%)" } }
    ),
    state(
      "visible",
      style({
        transform: "{{translateVisible}}",
      }),
      { params: { translateVisible: "translateX(0)" } }
    ),
    transition(
      "hidden => visible",
      animate("800ms cubic-bezier(.15,1.52,.53,1.03)")
    ),
  ]),
];

export const SlideAnimationSmooth = [
  trigger("slideAnim", [
    state(
      "hidden",
      style({
        transform: "{{translateHidden}}",
      }),
      { params: { translateHidden: "translateX(-100%)" } }
    ),
    state(
      "visible",
      style({
        transform: "{{translateVisible}}",
      }),
      { params: { translateVisible: "translateX(0)" } }
    ),
    transition("hidden => visible", animate("1300ms ease-out")),
    transition("visible => hidden", animate("800ms ease-in")),
  ]),
];
