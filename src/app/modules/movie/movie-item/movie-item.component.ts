import { Component, Input, OnInit } from "@angular/core";
import { IMovie } from "../../../data/interfaces/movie.interface";
import { ActiveGenreService } from "../../../shared/services/active-genre.service";
import { TGenre } from "../../../data/types/genre.type";
import { MovieService } from "../../../data/services/movie.service";
import { MovieListConfig } from "../../../static/config";

@Component({
  selector: "app-movie-item",
  templateUrl: "./movie-item.component.html",
  styleUrls: ["./movie-item.component.scss"],
})
export class MovieItemComponent implements OnInit {
  private activeGenre: string;
  @Input() movie: IMovie;

  constructor(
    private movieService: MovieService,
    private genre: ActiveGenreService
  ) {}

  ngOnInit() {}

  happyMovie(rate: string): boolean {
    return parseFloat(rate) > MovieListConfig.movieHappiness ? true : false;
  }

  getBGImage(obj: IMovie): string {
    return this.movieService.getMovieImageCSSUrlAttr(obj);
  }

  setActiveGenre(genre: TGenre): void {
    if (this.activeGenre !== genre) {
      this.genre.active = genre;
    }
  }
}
