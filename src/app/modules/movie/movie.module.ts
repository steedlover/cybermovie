import { NgModule } from "@angular/core";

import { MovieRoutingModule } from "./movie.routing";

import { SharedModule } from "../../shared/shared.module";

import { MovieListComponent } from "./pages/movie-list/movie-list.component";
import { MovieService } from "src/app/data/services/movie.service";
import { MovieItemComponent } from "./movie-item/movie-item.component";
import { MovieDetailsComponent } from "./pages/movie-details/movie-details.component";

@NgModule({
  declarations: [MovieListComponent, MovieItemComponent, MovieDetailsComponent],
  imports: [SharedModule, MovieRoutingModule],
  providers: [MovieService],
})
export class MovieModule {}
