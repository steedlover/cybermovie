import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { MovieListComponent } from "./pages/movie-list/movie-list.component";
import { MovieDetailsComponent } from "./pages/movie-details/movie-details.component";

export const routes: Routes = [
  {
    path: "",
    redirectTo: "movie",
    pathMatch: "full",
  },
  {
    path: "movie",
    component: MovieListComponent,
  },
  {
    path: "movie/:alias",
    component: MovieDetailsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MovieRoutingModule {}
