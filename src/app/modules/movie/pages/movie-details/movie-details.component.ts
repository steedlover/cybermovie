import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { MovieService } from "../../../../data/services/movie.service";
import { IMovie } from "../../../../data/interfaces/movie.interface";
import { MovieListConfig } from "../../../../static/config";

@Component({
  selector: "app-movie-details",
  templateUrl: "./movie-details.component.html",
  styleUrls: ["./movie-details.component.scss"],
})
export class MovieDetailsComponent implements OnInit {
  private alias: string;
  public movie: IMovie;
  public error: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private movieService: MovieService
  ) {}

  private getMovie(alias: string) {
    this.movieService.getMovieByAlias(alias).subscribe(
      (movie: IMovie) => {
        this.movie = movie;
        console.log(this.movie, "movie");
      },
      (err) => (this.error = err)
    );
  }

  happyMovie(rate: string): boolean {
    return parseFloat(rate) > MovieListConfig.movieHappiness ? true : false;
  }

  getBGImage(obj: IMovie): string {
    return this.movieService.getMovieImageCSSUrlAttr(obj);
  }

  ngOnInit() {
    this.route.paramMap.subscribe((paramMap) => {
      this.alias = paramMap.get("alias");
      this.getMovie(this.alias);
    });
  }
}
