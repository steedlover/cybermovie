import { Component, OnInit, OnDestroy } from "@angular/core";
import {
  trigger,
  style,
  transition,
  animate,
  query,
  stagger,
} from "@angular/animations";
import { Subscription, Subject } from "rxjs";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";
import { MovieService } from "../../../../data/services/movie.service";
import { ActiveGenreService } from "../../../../shared/services/active-genre.service";
import { IMovie } from "../../../../data/interfaces/movie.interface";
import { TGenre } from "../../../../data/types/genre.type";
import { TSort } from "../../../../data/types/sort.type";

@Component({
  selector: "app-movie-list",
  templateUrl: "./movie-list.component.html",
  styleUrls: ["./movie-list.component.scss"],
  animations: [
    trigger("fadeAnimList", [
      transition("* => new", [
        query(
          ".movie-item-box",
          style({
            opacity: 0,
            transform: "translateY(-30px)",
          }),
          // COMMENT
          // I added this here to avoid errors if items what the animation applied to
          // are undefined yet. But basically we should avoid the situations where
          // we apply animations to an empty list
          // that's why I have *ngIf directive in the HTML template
          // which doesn't allow these situations happen
          { optional: true }
        ),
        query(
          ".movie-item-box",
          stagger("100ms", [
            animate(
              "300ms",
              style({
                opacity: 1,
                transform: "translateY(0)",
              })
            ),
          ]),
          // COMMENT
          // I added this here to avoid errors if items what the animation applied to
          // are undefined yet. But basically we should avoid the situations where
          // we apply animations to an empty list
          // that's why I have *ngIf directive in the HTML template
          // which doesn't allow these situations happen
          { optional: true }
        ),
      ]),
    ]),
  ],
})
export class MovieListComponent implements OnInit, OnDestroy {
  public activeGenre: TGenre;
  private _movies: IMovie[];
  private _sorting: TSort;
  private allMovies: IMovie[];
  private subscriptions: Subscription[] = [];
  public moviesListState: string;
  public searchText: string;
  public debouncedInputValue = this.searchText;
  private searchDecouncer$: Subject<string> = new Subject();

  constructor(
    private genre: ActiveGenreService,
    private movieService: MovieService
  ) {}

  get movies(): IMovie[] {
    return this._movies;
  }

  set movies(newVal: IMovie[]) {
    this._movies = newVal;
  }

  get sortValue(): TSort {
    return this._sorting;
  }

  set sortValue(newVal: TSort) {
    if (newVal !== this._sorting) {
      this._sorting = newVal;
    }
  }

  public updateSortValue(newVal: TSort) {
    this.sortValue = newVal;
    this.updateMoviesList(this.sortMoviesList(this.movies, newVal));
  }

  public onSearchTextChange(search: string): void {
    this.searchDecouncer$.next(search);
  }

  private setupSearchDebouncer(): void {
    this.subscriptions.push(
      this.searchDecouncer$
        .pipe(debounceTime(500), distinctUntilChanged())
        .subscribe((search: string) => {
          this.debouncedInputValue = search;

          this.searchByText(search);
        })
    );
  }

  private searchByText(search: string): void {
    const currentGenreMovies = this.allMovies.filter(
        (movie) => movie.genres.indexOf(this.activeGenre) >= 0
      ),
      filtered = this.sortMoviesList(currentGenreMovies, this._sorting).filter(
        (movie) => movie.name.toLowerCase().indexOf(search.toLowerCase()) >= 0
      );
    this.updateMoviesList(filtered);
  }

  private sortByNumber(key) {
    return (a, b) => parseFloat(b[key]) - parseFloat(a[key]);
  }

  private sortByAlphabet(key) {
    return (a, b) => {
      if (a[key] > b[key]) {
        return 1;
      }
      if (a[key] < b[key]) {
        return -1;
      }
      return 0;
    };
  }

  private sortMoviesList(arr: IMovie[], key: TSort): IMovie[] {
    let result = [],
      sortFunction;

    switch (key) {
      case "rate":
        sortFunction = this.sortByNumber(key);
        break;
      case "name":
      default:
        sortFunction = this.sortByAlphabet(key);
        break;
    }
    result = arr.map((e) => e).sort(sortFunction);

    return result;
  }

  private setMoviesList(arr: IMovie[]): IMovie[] {
    let result = [];
    // COMMENT
    // if activeGenre is undefined, this will be
    // top rated movies list
    if (!this.activeGenre) {
      result = arr.sort(this.sortByNumber("rate")).slice(0, 5);
      this.sortValue = "rate";
    } else {
      result = arr
        .filter((movie) => movie.genres.indexOf(this.activeGenre) >= 0)
        .sort(this.sortByAlphabet("name"));
      this.sortValue = "name";
    }
    return result;
  }

  updateMoviesList(arr: IMovie[]) {
    this.moviesListState = "";
    // COMMENT
    // Without this hack animation renders old list first
    // and after that render the new list one more time
    // so I refresh the "list state" to prevent it
    // but I postpone this to the next cycle to make
    // it work properly
    this.movies = arr;
    setTimeout(() => (this.moviesListState = "new"), 0);
  }

  ngOnInit() {
    // COMMENT
    // Get an active genre if defined from the service
    if (this.genre.active) {
      this.activeGenre = this.genre.active;
    }

    // COMMENT
    // Init search debounce
    this.setupSearchDebouncer();

    this.movieService.getAllMovies().subscribe((movies: IMovie[]) => {
      this.allMovies = movies;
      this.updateMoviesList(this.setMoviesList(this.allMovies));
    });

    this.subscriptions.push(
      this.genre.activeGenreObserver.subscribe((genre: TGenre) => {
        if (this.activeGenre !== genre) {
          this.activeGenre = genre;
          this.updateMoviesList(this.setMoviesList(this.allMovies));
        }
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }
}
