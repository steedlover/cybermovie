import {
  Component,
  Inject,
  Renderer2,
  RendererFactory2,
  OnInit,
  OnDestroy,
} from "@angular/core";
import { DOCUMENT } from "@angular/common";
import { Observable, Subscription } from "rxjs";
import { Store, select } from "@ngrx/store";
import {
  IState,
  IStateResponse,
} from "../../../data/interfaces/state.interface";
import { map } from "rxjs/operators";

@Component({
  selector: "app-spinner",
  templateUrl: "./spinner.component.html",
  styleUrls: ["./spinner.component.scss"],
})
export class SpinnerComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  public _loading: boolean = false;
  private loading$: Observable<boolean>;
  private renderer: Renderer2;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private store: Store<IStateResponse>,
    private rendererFactory: RendererFactory2
  ) {}

  get loading(): boolean {
    return this._loading;
  }

  set loading(newVal: boolean) {
    if (newVal !== this._loading) {
      this._loading = newVal;

      if (this.loading === true) {
        this.renderer.addClass(this.document.body, "modal-open");
      } else {
        this.renderer.removeClass(this.document.body, "modal-open");
      }
    }
  }

  ngOnInit() {
    this.renderer = this.rendererFactory.createRenderer(null, null);
    this.loading$ = this.store.pipe(
      select((r: IStateResponse) => r.state),
      map((s: IState) => s.loading)
    );
    this.subscriptions.push(
      this.loading$.subscribe((l: boolean) => {
        console.log(l, "spinner component state");
        this.loading = l;
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }
}
