import { Injectable } from "@angular/core";
import { Subject, Observable } from "rxjs";
import { TGenre } from "../../data/types/genre.type";

@Injectable()
export class ActiveGenreService {
  private _active: TGenre;
  private active$ = new Subject<TGenre>();

  constructor() {}

  get activeGenreObserver(): Observable<TGenre> {
    return this.active$.asObservable();
  }

  set active(newVal: TGenre) {
    if (newVal !== this._active) {
      this._active = newVal;
      this.active$.next(this._active);
    }
  }

  get active(): TGenre {
    return this._active;
  }
}
