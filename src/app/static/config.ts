export const NavBarConfig = {
  navBarHeight: 92,
};

export const MovieListConfig = {
  imagesPath: "/assets/images/movie-covers/",
  keyStorageName: "sortKey",
  movieHappiness: 6,
};
