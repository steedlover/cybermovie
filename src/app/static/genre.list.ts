import { IGenre } from "../data/interfaces/genre.interface";

export const genreList: IGenre[] = [
  { name: "action", icon: "square" },
  { name: "adventure", icon: "square" },
  { name: "biography", icon: "book" },
  { name: "comedy", icon: "tasks" },
  { name: "crime", icon: "exclamation-triangle" },
  { name: "drama", icon: "language" },
  { name: "history", icon: "lightbulb" },
  { name: "mystery", icon: "paint-brush" },
  { name: "scifi", icon: "asterisk" },
  { name: "sport", icon: "stream" },
  { name: "thriller", icon: "window-maximize" },
];
